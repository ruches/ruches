## Une application web pour gérer des ruches

Développée avec Spring Boot, Tomcat et Postgresql.
Et aussi : Datatables, Bootstrap, OpenLayers, Chartjs, IGN géoplateforme...

En français et anglais.

License GPL-3.

## Compilation, déploiement.

```
git clone https://codeberg.org/ruches/ruches.git
cd ruches
mvn package -DskipTests
cp target/ruches.war.original /.../.../tomcat11/webapps/ruches.war
```

## Documentation

* https://codeberg.org/ooioo/ruches-doc/src/branch/master/podman pour tester Ruches avec Podman
* https://codeberg.org/ooioo/ruches-doc/src/branch/master/sql création de la base de données
* https://codeberg.org/ooioo/ruches-doc/src/branch/master/docs/install vhhost, context.xml ...
* https://codeberg.org/ooioo/ruches-doc la documentation

## Nos clients, ils nous font confiance !   :-)

https://les-abeilles-de-la-lisette.fr/wordpress/


